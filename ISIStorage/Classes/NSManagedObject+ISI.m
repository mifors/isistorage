//
//  NSManagedObject+ISI.m
//  Eureka inTouch
//
//  Created by Ivan Isaev on 12.04.17.
//  Copyright © 2017 Ivan Isaev. All rights reserved.
//

#import "NSManagedObject+ISI.h"
#import "ISIStorage+Entity.h"


@implementation NSManagedObject (ISI)

#pragma mark -
#pragma mark Пользовательские настройки

+ (NSArray *)isi_defaultSortDescriptorsForAll
{
    return nil;
}

+ (NSString *)isi_remoteIDKey
{
    return @"serverId";
}


#pragma mark -
#pragma mark - Имя класса

+ (NSString *)isi_entityName
{
    return NSStringFromClass([self class]);
}

- (NSString *)isi_entityName
{
    return NSStringFromClass([self class]);
}


#pragma mark -
#pragma mark - Новая сущность

+ (__kindof NSManagedObject *)isi_newEntity
{
    return [ISIStorage newEntityForName:[self isi_entityName]];
}


#pragma mark -
#pragma mark - Все сущности

+ (NSArray *)isi_all
{
    return [self isi_allWithSortDescriptors:[self isi_defaultSortDescriptorsForAll]];
}

+ (NSArray *)isi_allSortedBy:(NSString *)key
{
    return [self isi_allSortedBy:key ascending:YES];
}

+ (NSArray *)isi_allReversedBy:(NSString *)key
{
    return [self isi_allSortedBy:key ascending:NO];
}

+ (NSArray *)isi_allSortedBy:(NSString *)key ascending:(BOOL)asc
{
    NSArray *sorter = key.length > 0 ? @[[NSSortDescriptor sortDescriptorWithKey:key ascending:asc]] : nil;
    return [self isi_allWithSortDescriptors:sorter];
}

+ (NSArray *)isi_allWithSortDescriptors:(NSArray *)descriptors
{
    return [ISIStorage allEntitiesForName:[self isi_entityName] sortedBy:descriptors];
}


#pragma mark -
#pragma mark Удалить все

+ (void)isi_deleteAll
{
    [ISIStorage deleteAllEntitiesForName:[self isi_entityName]];
}


#pragma mark -
#pragma mark - Поиск сущностей

+ (__kindof NSManagedObject *)isi_findByRemoteID:(id)remoteID
{
    return [self isi_findByKey:[self isi_remoteIDKey] withValue:remoteID];
}

+ (__kindof NSManagedObject *)isi_findByKey:(NSString *)key withValue:(id)value
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", key, value];
    return [ISIStorage findEntityForName:[self isi_entityName] withPredicate:predicate];
}

+ (__kindof NSManagedObject *)isi_findOrCreateWithRemoteID:(id)remoteID
{
    return [self isi_findOrCreateWithKey:[self isi_remoteIDKey] andValue:remoteID];
}

+ (__kindof NSManagedObject *)isi_findOrCreateWithKey:(NSString *)key andValue:(id)value
{
    if (key.length > 0 && value != nil) {
        __kindof NSManagedObject *obj = [self isi_findByKey:key withValue:value] ?: [self isi_newEntity];
        if (obj) {
            [obj setValue:value forKey:key];
        }
        return obj;
    }
    return nil;
}

@end
