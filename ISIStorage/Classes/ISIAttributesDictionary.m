//
//  ISIAttributesDictionary.m
//  Eureka inTouch
//
//  Created by Ivan Isaev on 13.04.17.
//  Copyright © 2017 Ivan Isaev. All rights reserved.
//

#import "ISIAttributesDictionary.h"

@implementation ISIAttributesDictionary

+ (Class)transformedValueClass
{
    return [NSDictionary class];
}

+ (BOOL)allowsReverseTransformation
{
    return YES;
}

- (id)transformedValue:(id)value
{
    return [NSKeyedArchiver archivedDataWithRootObject:value];
}

- (id)reverseTransformedValue:(id)value
{
    return [NSKeyedUnarchiver unarchiveObjectWithData:value];
}

@end
