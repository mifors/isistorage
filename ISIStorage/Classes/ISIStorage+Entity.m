//
//  ISIStorage+Entity.m
//  ISIStorage
//
//  Created by Ivan Isaev on 14.08.17.
//  Copyright © 2017 Ivan Isaev. All rights reserved.
//

#import "ISIStorage+Entity.h"

@implementation ISIStorage (Entity)

+ (__kindof NSManagedObject *)newEntityForName:(NSString *)entityName
{
    return [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:[ISIStorageSI defaultContext]];
}


#pragma mark -
#pragma mark Find All

+ (NSArray *)allEntitiesForName:(NSString *)entityName
{
    return [self allEntitiesForName:entityName sortedBy:nil withPredicate:nil];
}

+ (NSArray *)allEntitiesForName:(NSString *)entityName sortedBy:(NSArray *)sortDescriptors
{
    return [self allEntitiesForName:entityName sortedBy:sortDescriptors withPredicate:nil];
}

+ (NSArray *)allEntitiesForName:(NSString *)entityName withPredicate:(NSPredicate *)predicate
{
    return [self allEntitiesForName:entityName sortedBy:nil withPredicate:predicate];
}

+ (NSArray *)allEntitiesForName:(NSString *)entityName sortedBy:(NSArray *)sortDescriptors withPredicate:(NSPredicate *)predicate
{
    NSFetchRequest *fetchRequest = [NSFetchRequest new];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:ISIStorageSI.defaultContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    
    if (sortDescriptors) {
        fetchRequest.sortDescriptors = sortDescriptors;
    }
    
    NSError *error = nil;
    return [ISIStorageSI.defaultContext executeFetchRequest:fetchRequest error:&error];
}


#pragma mark -
#pragma mark Delete All

+ (void)deleteAllEntitiesForName:(NSString *)entityName
{
    NSArray *all = [self allEntitiesForName:entityName];
    for (__kindof NSManagedObject *oneObj in all) {
        [ISIStorageSI.defaultContext deleteObject:oneObj];
    }
}


#pragma mark -
#pragma mark Поиск

+ (__kindof NSManagedObject *)findEntityForName:(NSString *)entityName withPredicate:(NSPredicate *)predicate
{
    NSFetchRequest *fetchRequest = [NSFetchRequest new];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:ISIStorageSI.defaultContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *list = [ISIStorageSI.defaultContext executeFetchRequest:fetchRequest error:&error];
    
    return (list.count > 0) ? [list firstObject] : nil;
}

@end
