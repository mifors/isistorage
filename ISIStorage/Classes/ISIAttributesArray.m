//
//  ISIAttributesArray.m
//  Eureka inTouch
//
//  Created by Ivan Isaev on 13.04.17.
//  Copyright © 2017 Ivan Isaev. All rights reserved.
//

#import "ISIAttributesArray.h"

@implementation ISIAttributesArray

+ (Class)transformedValueClass
{
    return [NSArray class];
}

+ (BOOL)allowsReverseTransformation
{
    return YES;
}

- (id)transformedValue:(id)value
{
    return [NSKeyedArchiver archivedDataWithRootObject:value];
}

- (id)reverseTransformedValue:(id)value
{
    return [NSKeyedUnarchiver unarchiveObjectWithData:value];
}

@end
