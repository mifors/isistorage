//
//  ISIStorage+Entity.h
//  ISIStorage
//
//  Created by Ivan Isaev on 14.08.17.
//  Copyright © 2017 Ivan Isaev. All rights reserved.
//

#import "ISIStorage.h"

@interface ISIStorage (Entity)

+ (__kindof NSManagedObject *)newEntityForName:(NSString *)entityName;

+ (NSArray *)allEntitiesForName:(NSString *)entityName;
+ (NSArray *)allEntitiesForName:(NSString *)entityName sortedBy:(NSArray *)sortDescriptors;
+ (NSArray *)allEntitiesForName:(NSString *)entityName withPredicate:(NSPredicate *)predicate;
+ (NSArray *)allEntitiesForName:(NSString *)entityName sortedBy:(NSArray *)sortDescriptors withPredicate:(NSPredicate *)predicate;

+ (void)deleteAllEntitiesForName:(NSString *)entityName;

+ (__kindof NSManagedObject *)findEntityForName:(NSString *)entityName withPredicate:(NSPredicate *)predicate;


@end
