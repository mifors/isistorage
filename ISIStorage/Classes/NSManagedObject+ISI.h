//
//  NSManagedObject+ISI.h
//  Eureka inTouch
//
//  Created by Ivan Isaev on 12.04.17.
//  Copyright © 2017 Ivan Isaev. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (ISI)

+ (NSArray *)isi_defaultSortDescriptorsForAll;
+ (NSString *)isi_remoteIDKey;

+ (NSString *)isi_entityName;
- (NSString *)isi_entityName;

+ (__kindof NSManagedObject *)isi_newEntity;

+ (NSArray *)isi_all;
+ (NSArray *)isi_allSortedBy:(NSString *)key;
+ (NSArray *)isi_allSortedBy:(NSString *)key ascending:(BOOL)asc;
+ (NSArray *)isi_allReversedBy:(NSString *)key;
+ (NSArray *)isi_allWithSortDescriptors:(NSArray *)descriptors;

+ (void)isi_deleteAll;

+ (__kindof NSManagedObject *)isi_findByRemoteID:(id)remoteID;
+ (__kindof NSManagedObject *)isi_findByKey:(NSString *)key withValue:(id)value;

+ (__kindof NSManagedObject *)isi_findOrCreateWithRemoteID:(id)remoteID;
+ (__kindof NSManagedObject *)isi_findOrCreateWithKey:(NSString *)key andValue:(id)value;

@end
