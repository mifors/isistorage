//
//  ISIStorage.h
//  Eureka inTouch
//
//  Created by Ivan Isaev on 12.04.17.
//  Copyright © 2017 Ivan Isaev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "NSManagedObject+ISI.h"

//! Project version number for ISIStorage.
FOUNDATION_EXPORT double ISIStorageVersionNumber;

//! Project version string for ISIStorage.
FOUNDATION_EXPORT const unsigned char ISIStorageVersionString[];

#define ISIStorageSI    [ISIStorage sharedInstance]
@interface ISIStorage : NSObject

+ (ISIStorage *)sharedInstance;
- (void)configureWithModelName:(NSString *)modelName;

- (NSManagedObjectContext *)defaultContext;
- (void)saveContext;
- (void)flushDatabase;

- (NSURL *)applicationDocumentsDirectory;

@end
